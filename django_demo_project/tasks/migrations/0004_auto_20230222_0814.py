# Generated by Django 3.2.16 on 2023-02-22 08:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0003_userdevice'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userdevice',
            options={'verbose_name': 'User Device', 'verbose_name_plural': 'User Devices'},
        ),
        migrations.RemoveField(
            model_name='userdevice',
            name='date',
        ),
        migrations.RemoveField(
            model_name='userdevice',
            name='user_agent',
        ),
        migrations.AddField(
            model_name='userdevice',
            name='device_name',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='userdevice',
            name='last_login',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='userdevice',
            name='ip_address',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
