from django.contrib import admin

from tasks.models import FailedLogin, UserDeviceInfo, UserDevice
from django.contrib.auth.models import User
# Register your models here.


class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "username")


class CustomeUserdeviceAdmin(admin.ModelAdmin):
    list_display = ("id", "user", "device_name", "ip_address", "last_login")


admin.site.register(UserDeviceInfo)
admin.site.register(UserDevice, CustomeUserdeviceAdmin)
admin.site.register(FailedLogin)
