from rest_framework import routers, serializers, viewsets
from .models import UserDevice, UserDeviceInfo
import datetime


class UserDeviceInfoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserDeviceInfo
        fields = ['id', 'device_name', 'ip_address', 'created_at']


class UserDeviceSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    user_device = serializers.StringRelatedField()

    class Meta:
        model = UserDevice
        fields = ('id', 'username','user_device', 'device_name', 'ip_address', 'last_login')

    def get_username(self, user_device):
        return user_device.user.username
