import datetime

from tasks.models import UserDeviceInfo


class SaveIpAddressMiddleware(object):
    """
        Save the Ip address if does not exist
    """

    def process_request(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[-1].strip()
        else:
            ip = request.META.get('REMOTE_ADDR')
        try:
            UserDeviceInfo.objects.get(ip_address=ip)
        except UserDeviceInfo.DoesNotExist:  # -----Here My Edit
            ip_address = UserDeviceInfo(
                ip_address=ip, created_at=datetime.datetime.now())
            ip_address.save()
            return None
