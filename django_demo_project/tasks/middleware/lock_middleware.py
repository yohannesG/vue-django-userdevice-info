# middleware.py

from django_demo_project import settings
from tasks.models import FailedLogin
from django.contrib.auth import get_user_model
from django.contrib.auth.signals import user_login_failed, user_logged_in
from django.dispatch import receiver
from django.utils import timezone
from tasks.celery.celery_tasks import lock_user_account
import json
from django.core.cache import cache
from datetime import timedelta
User = get_user_model()


class LockMiddleware:
    def __init__(self, get_response) -> None:
        self.get_response = get_response

    def __call__(self, request):
        print(request.body)
        
        # if request.body :
        #     encoding = 'utf-8'
        #     user_data_str = request.body.decode(encoding)
        #     user_data = json.loads(user_data_str)
        #     # response = self.get_response(request)
            
        #     # if request.method == 'POST' and 'login' in request.POST:
        #     username = user_data['username']
        #     password = user_data['password']
        #     print(username, password)
        #     user = User.objects.filter(username=username).first()
        #     print(user.is_active)
        #     if user and not user.is_active:
        #         return self.get_response(request)
        #     if not user or not user.check_password(password):
        #         self.increment_failed_attempts(username)
        #         if self.get_failed_attempts(username) >= settings.MAX_LOGIN_ATTEMPTS:
        #             self.lock_user_account(user)
        #         return self.get_response(request)
        #     self.reset_failed_attempts(username)
        return self.get_response(request)

    def increment_failed_attempts(self, username):
        key = f'{username}_attempts'
        attempts = cache.get(key)
        if attempts is not None:
            attempts += 1
        else:
            attempts = 1
        cache.set(key, attempts, timeout=timedelta(minutes=5).seconds)

    def get_failed_attempts(self, username):
        key = f'{username}_attempts'
        attempts = cache.get(key)
        return attempts if attempts is not None else 0

    def reset_failed_attempts(self, username):
        key = f'{username}_attempts'
        cache.delete(key)

    def lock_user_account(self, user):
        user.is_active = False
        user.save()
        lock_user_account.apply_async(
            args=[user.id], countdown=timedelta(minutes=5).seconds)

