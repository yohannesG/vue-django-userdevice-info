from django.shortcuts import render
from rest_framework import generics
from tasks.mixins import UserDeviceMixin
from tasks.models import UserDevice, UserDeviceInfo
from tasks.serializers import UserDeviceInfoSerializer, UserDeviceSerializer
import datetime
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework_simplejwt.views import TokenObtainPairView

from rest_framework.mixins import ListModelMixin
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from .mixins import UserDeviceMixin
from django.contrib.auth.models import User
from rest_framework.pagination import CursorPagination
from rest_framework.pagination import LimitOffsetPagination

class LoginView(TokenObtainPairView, UserDeviceMixin):
    authentication_classes = [TokenAuthentication]

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        if serializer.is_valid():
            print(serializer.validated_data)
            print(request.data["username"])
            user = User.objects.get(username=request.data["username"])
            print(user)
            self.login()
        return super().post(request, *args, **kwargs)


class UserDeviceInfoListCreateView(UserDeviceMixin, generics.ListCreateAPIView, ListModelMixin):
    queryset = UserDevice.objects.all()
    serializer_class = UserDeviceSerializer
    authentication_classes = [TokenAuthentication]
    # pagination_class = [LimitOffsetPagination]

class UserDeviceInfoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UserDevice.objects.all()
    serializer_class = UserDeviceSerializer
