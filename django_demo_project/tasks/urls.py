from django import views
from django.urls import include, path
from .views import UserDeviceInfoListCreateView, UserDeviceInfoDetail, LoginView
from rest_framework_simplejwt.views import TokenRefreshView

urlpatterns = [
    path('tasks/', UserDeviceInfoListCreateView.as_view()),
    path('tasks/<int:pk>/', UserDeviceInfoDetail.as_view()),
    path('login', LoginView.as_view()),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
