# tasks.py

from celery import shared_task
from django.contrib.auth import get_user_model


@shared_task
def lock_user_account(user_id):
    UserModel = get_user_model()
    try:
        user = UserModel.objects.get(id=user_id)
        user.is_active = False
        user.failed_login_attempts = 0
        user.save(update_fields=['is_active', 'failed_login_attempts'])
    except UserModel.DoesNotExist:
        pass


@shared_task
def unlock_user_accounts():
    UserModel = get_user_model()
    UserModel.objects.filter(is_active=False).update(is_active=True)
