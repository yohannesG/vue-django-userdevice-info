from rest_framework import mixins, viewsets
from django.utils import timezone
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from .models import UserDevice
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User


class UserDeviceMixin:
    def perform_login(self, user):
        # Record the user's device information

        device_name = self.request.META.get(
            'HTTP_USER_AGENT', 'Unknown Device')
        ip_address = self.request.META.get('REMOTE_ADDR', '0.0.0.0')
        last_login = timezone.now()

        user_device_data = UserDevice.objects.filter(user=user.id,
                                                     device_name=device_name, ip_address=ip_address).count()

        if user_device_data == 0:
            data = {"user": user, "device_name": device_name,
                    "ip_address": ip_address, "last_login": last_login}
            print(data)
            UserDevice.objects.create(**data)
        else:
            self.check_user_location(user)

    def check_user_location(self, user):
        request = self.request
        # Check if the user's IP address has changed since their last login
        current_ip_address = self.request.META.get('REMOTE_ADDR', '0.0.0.0')
        try:
            device = UserDevice.objects.filter(user=user.id)
            print("device", device)
            if device[0].ip_address != request.META.get('REMOTE_ADDR'):
                send_mail(
                    'Warning: Attempt to login from different location',
                    f'There is an attempt to login your account from {request.META.get("REMOTE_ADDR")}',
                    'from@example.com',
                    [user.email],
                    fail_silently=False,
                )
        except UserDevice.DoesNotExist:
            pass

    def login(self):
        # Call the parent login method to perform the actual login

        # Record the user's device information and check their location
        user = User.objects.get(username=self.request.data["username"])

        # self.check_user_location(user)

        # Return the response from the parent login method
        return self.perform_login(user)
